struct Import
{
    wchar_t* name;
    
    Import* next;
};

struct SxsAssembly
{
    wchar_t* name;
    wchar_t* version;
    wchar_t* processorArchitecture;
    
    SxsAssembly* next;
};

enum BinDepStatusCodes
{   
    UnknownError = -1,
    InvalidParameter = -2,
    InvalidFile = -3,
};

#ifndef BINDEP_EXPORT

extern "C" __declspec(dllimport) int GetImports(const wchar_t* filename, Import** imports);
extern "C" __declspec(dllimport) int GetSxsAssemblies(const wchar_t* filename, SxsAssembly** sxsAssemblies);

extern "C" __declspec(dllimport) int FreeImports(Import* imports);
extern "C" __declspec(dllimport) int FreeSxsAssemblies(SxsAssembly* sxsAssemblies);

#endif