#include <windows.h>
#include <pugixml.hpp>
#include <vector>
#include <string>

#define BINDEP_EXPORT
#include "..\include\BinDep.h"

using pugi::xml_document;
using pugi::xml_parse_result;
using pugi::xpath_node_set;

using std::vector;

using std::string;
using std::wstring;

struct Manifest
{
    void* bytes;
    int size;
};

BOOL CALLBACK EnumResourceNamesCallback(HMODULE module, LPCTSTR type, LPTSTR name, LONG_PTR param)
{
    auto manifests = (vector<Manifest>*)param;
    if (manifests == nullptr)
    {
        return FALSE;
    }

    HRSRC resource = FindResource(module, name, type);
    HGLOBAL data = LoadResource(module, resource);
    LPVOID bytes = LockResource(data);
    DWORD size = SizeofResource(module, resource);

    Manifest manifest;
    manifest.bytes = bytes;
    manifest.size = size;

    manifests->push_back(manifest);

    return TRUE;
}

extern "C" __declspec(dllexport) int GetSxsAssemblies(const wchar_t* filename, SxsAssembly** sxsAssemblies)
{
    if (filename == nullptr || sxsAssemblies == nullptr)
    {
        return InvalidParameter;
    }

    *sxsAssemblies = nullptr;

    HMODULE module = LoadLibraryEx(filename, nullptr, LOAD_LIBRARY_AS_IMAGE_RESOURCE | LOAD_LIBRARY_AS_DATAFILE);
    if (module == nullptr)
    {
        return InvalidFile;
    }

    vector<Manifest> manifests;
    EnumResourceNames(module, MAKEINTRESOURCE(RT_MANIFEST), EnumResourceNamesCallback, (LONG_PTR)&manifests);

    SxsAssembly* pCurSxsAssembly = nullptr;
    for (auto itm = manifests.begin(); itm != manifests.end(); ++itm)
    {
        auto& manifest = *itm;

        xml_document doc;
        xml_parse_result result = doc.load_buffer(manifest.bytes, manifest.size);

        xpath_node_set assemblies = doc.select_nodes(L"/assembly/dependency/dependentAssembly");
        for (auto ita = assemblies.begin(); ita != assemblies.end(); ++ita)
        {
            auto& assembly = *ita;

            auto identity = assembly.node().child(L"assemblyIdentity");
            auto name = identity.attribute(L"name").value();
            auto version = identity.attribute(L"version").value();
            auto processorArchitecture = identity.attribute(L"processorArchitecture").value();
            auto bob = identity.attribute(L"bob").value();

            wchar_t* outName = new wchar_t[wcslen(name) + 1];
            wcscpy_s(outName, wcslen(name) + 1, name);

            wchar_t* outVersion = new wchar_t[wcslen(version) + 1];
            wcscpy_s(outVersion, wcslen(version) + 1, version);

            wchar_t* outProcessorArchitecture = new wchar_t[wcslen(processorArchitecture) + 1];
            wcscpy_s(outProcessorArchitecture, wcslen(processorArchitecture) + 1, processorArchitecture);

            SxsAssembly* newSxsAssembly = new SxsAssembly;
            newSxsAssembly->name = outName;
            newSxsAssembly->version = outVersion;
            newSxsAssembly->processorArchitecture = outProcessorArchitecture;
            newSxsAssembly->next = nullptr;

            if (pCurSxsAssembly != nullptr)
            {
                pCurSxsAssembly->next = newSxsAssembly;
                pCurSxsAssembly = newSxsAssembly;
            }
            else
            {
                pCurSxsAssembly = newSxsAssembly;
                *sxsAssemblies = newSxsAssembly;
            }
        }

    }

    FreeLibrary(module);

    return 0;
}

extern "C" __declspec(dllexport) int FreeSxsAssemblies(SxsAssembly* sxsAssemblies)
{
    if (sxsAssemblies == nullptr)
    {
        return InvalidParameter;
    }

    SxsAssembly* pCurSxsAssembly = sxsAssemblies;
    for (; pCurSxsAssembly != nullptr;)
    {
        SxsAssembly* next = pCurSxsAssembly->next;

        delete[] pCurSxsAssembly->name;
        delete[] pCurSxsAssembly->version;
        delete[] pCurSxsAssembly->processorArchitecture;
        delete pCurSxsAssembly;

        pCurSxsAssembly = next;
    }

    return 0;
}
