#include <Windows.h>

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "../../include/BinDep.h"

using std::string;
using std::wstring;
using std::vector;
using std::wcout;
using std::endl;
using std::ofstream;

string wstringToString(const wstring& s)
{
    return string(s.begin(), s.end());
}

int wmain(int argc, wchar_t* argv[])
{
    if (argc < 2)
        return -1;

    bool quiet = false;
    bool csv = false;
    ofstream csvFile;
    for (int i = 1; i < argc; ++i)
    {
        // Quiet mode, disable output
        if (wcscmp(argv[i], L"-q") == 0 || wcscmp(argv[i], L"--quiet") == 0)
        {
            quiet = true;
        }

        if (wcscmp(argv[i], L"-c") == 0 || wcscmp(argv[i], L"--csv") == 0)
        {
            if (i < argc + 2)
            {
                csv = true;
                csvFile.open(argv[i + 1]);
                csvFile << "File,Type,Name,Version,Architecture" << endl;
            }
        }
    }

    if (!quiet)
    {
        wcout << "BINDEP Build 2015/08/31" << endl << endl;;
    }

    vector<wstring> files;
    wchar_t* next = nullptr;
    wchar_t* token = wcstok_s(argv[argc - 1], L";", &next);
    while (token)
    {
        files.push_back(token);
        token = wcstok_s(nullptr, L";", &next);
    }

    for (auto itr = files.begin(); itr != files.end(); ++itr)
    {
        if (!quiet)
        {
            wcout << "File: " << *itr << endl;
        }

        Import* imports;
        if (GetImports(itr->c_str(), &imports) != 0)
        {
            return -1;
        }

        SxsAssembly* sxsAssemblies;
        if (GetSxsAssemblies(itr->c_str(), &sxsAssemblies) != 0)
        {
            return -1;
        }

        if (!quiet)
        {
            wcout << "    Imports:" << endl;
        }

        Import* curImport = imports;
        while (curImport != nullptr)
        {
            if (!quiet)
            {
                wcout << "        " << curImport->name << endl;
            }

            if (csv)
            {
                csvFile << wstringToString(*itr) << ",Import," << wstringToString(curImport->name) << ",," << endl;
            }

            curImport = curImport->next;
        }

        if (!quiet)
        {
            wcout << endl << "    Side-by-side:" << endl;
        }

        SxsAssembly* curSxsAssembly = sxsAssemblies;
        while (curSxsAssembly != nullptr)
        {
            if (!quiet)
            {
                wcout << "        " << curSxsAssembly->name
                    << "    " << curSxsAssembly->version
                    << "    " << curSxsAssembly->processorArchitecture << endl;
            }

            if (csv)
            {
                csvFile << wstringToString(*itr)
                    << ",Side-by-side,"
                    << wstringToString(curSxsAssembly->name) << ","
                    << wstringToString(curSxsAssembly->version) << ","
                    << wstringToString(curSxsAssembly->processorArchitecture) << endl;
            }

            curSxsAssembly = curSxsAssembly->next;
        }

        if (!quiet)
        {
            wcout << endl;
        }
        
        FreeImports(imports);
        FreeSxsAssemblies(sxsAssemblies);
    }

    return 0;
}