#include <Windows.h>
#include <DbgHelp.h>

#include <string>
#include <list>

#define BINDEP_EXPORT
#include "..\include\BinDep.h"

#pragma comment(lib, "DbgHelp.lib")

using std::wstring;
using std::list;


extern "C" __declspec(dllexport) int GetImports(const wchar_t* filename, Import** imports)
{
    HANDLE hFile = nullptr;
    HANDLE hMapping = nullptr;
    VOID* pBase = nullptr;

    if (filename == nullptr || imports == nullptr)
    {
        return InvalidParameter;
    }

    *imports = nullptr;

    hFile = CreateFile(filename,
        GENERIC_READ,
        FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE,
        nullptr,
        OPEN_EXISTING,
        FILE_ATTRIBUTE_NORMAL,
        nullptr);
    if (hFile == INVALID_HANDLE_VALUE)
    {
        return InvalidFile;
    }

    hMapping = CreateFileMapping(hFile, nullptr, PAGE_READONLY, 0, 0, nullptr);
    if (hMapping == INVALID_HANDLE_VALUE)
    {
        goto Fail;
    }

    pBase = MapViewOfFile(hMapping, FILE_MAP_READ, 0, 0, 0);
    if (pBase == nullptr)
    {
        goto Fail;
    }

    IMAGE_NT_HEADERS* pNtHeaders = ImageNtHeader(pBase);
    if (pNtHeaders == nullptr)
    {
        goto Fail;
    }

    ULONG dataSize;
    void* pData = ImageDirectoryEntryToDataEx(pBase, FALSE, IMAGE_DIRECTORY_ENTRY_IMPORT, &dataSize, nullptr);
    if (pData == nullptr)
    {
        goto Fail;
    }

    Import* pCurImport = nullptr;
    
    IMAGE_IMPORT_DESCRIPTOR* pFirst = (IMAGE_IMPORT_DESCRIPTOR*)pData;
    IMAGE_IMPORT_DESCRIPTOR* pDesc;
    for (pDesc = pFirst; pDesc->Characteristics; ++pDesc)
    {
        char* name = (char*)ImageRvaToVa(pNtHeaders, pBase, pDesc->Name, nullptr);
        if (name)
        {
            size_t n;
            mbstowcs_s(&n, nullptr, 0, name, 0);
            wchar_t* wName = new wchar_t[n];
            mbstowcs_s(nullptr, wName, n, name, n);

            Import* pNewInport = new Import;
            pNewInport->name = wName;
            pNewInport->next = nullptr;

            if (pCurImport != nullptr)
            {
                pCurImport->next = pNewInport;
                pCurImport = pNewInport;
            }
            else
            {
                pCurImport = pNewInport;
                *imports = pNewInport;
            }
        }
    }

    UnmapViewOfFile(pBase);
    CloseHandle(hMapping);
    CloseHandle(hFile);
    
    return 0;

Fail:
    
    if (pBase)
    {
        UnmapViewOfFile(pBase);
    }

    if (hMapping)
    {
        CloseHandle(hMapping);
    }

    if (hFile)
    {
        CloseHandle(hFile);
    }

    return UnknownError;
}

extern "C" __declspec(dllexport) int FreeImports(Import* imports)
{
    if (imports == nullptr)
    {
        return InvalidParameter;
    }

    Import* pCurImport = imports;
    for (; pCurImport != nullptr;)
    {
        Import* next = pCurImport->next;
        
        delete[] pCurImport->name;
        delete pCurImport;

        pCurImport = next;
    }

    return 0;
}